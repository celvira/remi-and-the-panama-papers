# Remi and the Papers of Panama

## Setup
Please follow these steps to setup the game

1. clone the gitlab repository inside your favority folder
    ```bash
    $ git clone https://gitlab.inria.fr/celvira/remi-and-the-panama-papers
    ```
2. cd in the code folder
    ```bash
    $ cd remi-and-panama-papers
    ```
3. Set up the virtual environment
    ```bash
    $ virtualenv venv -p python3
    ```
4. Activate the virtual enviroment
    ```bash
    $ source venv/bin/activate
    ```
5. Install the requireents
    ```bash
    (venv) $ pip install -r requirements.txt
    ```
6. Run the main and hope that everything is working fine. Otherwise call Jeremy or Macron
    ```bash
    (venv) $ python src/main.py
    ```
    Remember to run the code always from the parent folder, that is with src/ at
    the beginning.
    ```bash
    (venv) $ python src/main.py --not-print
    # or, in a shorter version, 
    (venv) $ python src/main.py -n
    ```

7. Have fun :)

## Submission
send an email to Diego (diego.di-carlo@inria.fr) with your your\_awesome\_team\_name_player.py file
