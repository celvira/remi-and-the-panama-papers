import numpy
import argparse
import importlib.util

from board import Board
from remi import Remi
from utils import load_from_yaml
import utils_ia

def load_player_from_module_path(path_to_module):
    spec = importlib.util.spec_from_file_location('module.name', path_to_module)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module

def main(args):
    params = load_from_yaml(args['game_params_yml'])
    my_player_module = load_player_from_module_path(args['player_file_name'])
    my_player = my_player_module.Player(name='my name', idx=0, cits = params['n_citations'])

    # Create game stuff
    board = Board()
    remi = Remi()
    list_citation_players = [15, 15]

    # dice can be something
    utils_ia.simulate_round(my_player, board, remi, list_citation_players, dice=None)


if __name__ == '__main__':
    args = {}
    args['game_params_yml'] = './data/game_params.yml'
    args['player_file_name'] = "./src/players/my_awesome_player.py"
    main(args)
