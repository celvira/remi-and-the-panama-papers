import numpy as np

from orientation import Orientation
from remi import Remi

class Board:
    """
        some doc about Board
    """
    NO_PAPER = -9
    NO_REMI  = -10

    def __init__(self, size=7, tiles=None):
        if tiles is None:
            self.tiles = self.NO_PAPER * np.ones((size, size),dtype=int)
        else:
            self.tiles = tiles.astype(int)
        self.size = size


    def get_tile_player(self,pos):
        """
            doc get_tile_player...
        """
        return self.tiles[pos[0],pos[1]]

    def copy(self):
        return Board(self.size, self.tiles.copy())

    def get_connex_paper(self, pos, index_player):
        return self._get_connex_paper([pos], index_player)


    def _get_connex_paper(self, indices, player):
        """
        This function takes the board as input,
        and supposes the set 'indices' is provided with the same label in the
        board. Then this function either return a new indices set all OK
        neighbours. If there are no OK neighbors, it stops.
        """
        indices_new = indices.copy()
        for l in indices:
            # Check all tiles in current convex set
            if l[0]>0:
                # If current tile is not top border, explore top
                if [l[0]-1, l[1]] not in indices_new:
                    if self.tiles[l[0]-1, l[1]] == player:
                        indices_new.append([l[0]-1, l[1]])

            if l[0]<self.size-1:
                # If current tile is not bottom border, explore below
                if [l[0]+1, l[1]] not in indices_new:
                    if self.tiles[l[0]+1, l[1]] == player:
                        indices_new.append([l[0]+1, l[1]])

            if l[1]>0:
                # If current tile is not left border, explore left
                if [l[0], l[1]-1] not in indices_new:
                    if self.tiles[l[0], l[1]-1] == player:
                        indices_new.append([l[0], l[1]-1])

            if l[1]<self.size-1:
                # If current tile is not right border, explore right
                if [l[0], l[1]+1] not in indices_new:
                    if self.tiles[l[0], l[1]+1] == player:
                        indices_new.append([l[0], l[1]+1])

        if len(indices_new)==len(indices):
            return len(indices_new)
        # If sizes have indeed changed, rerun with new convex initial set
        return self._get_connex_paper(indices_new, player)


    def add_paper(self, paper, index_player):
        tile1 = paper[0]
        tile2 = paper[1]
        self.tiles[tile1[0], tile1[1]] = index_player
        self.tiles[tile2[0], tile2[1]] = index_player


    def check_tile_in_board(self, pos):
        """
        return True if position tile is in the board and false otherwise

        Parameters
        ----------
        pos    : List
            a list of two elements containing the position of the tile

        Returns
        -------
        out: bool
            True if the pos is in the board and false otherwise
        """
        if pos[0] < 0 or pos[0] >= self.size:
            return False

        if pos[1] < 0 or pos[1] >= self.size:
            return False

        return True


    def check_tiles_are_contiguous(self, til1, til2):
        """
        return True if the two tile are next to each other
        and false otherwise

        Parameters
        ----------
        til1: List
            a list of two elements containing the position of tile1

        til2: List
            a list of two elements containing the position of tile2

        Returns
        -------
        out: bool
            True if the tiles are next to each other
        """
        diff_i = abs(til1[0] - til2[0])
        diff_j = abs(til1[1] - til2[1])

        if diff_i + diff_j is not 1:
            return False
        else:
            return True

    def move_remi(self, remi, dice):
        path = []
        for count in range(dice):
            self._move_remi(remi)
            path.append(remi.pos)
        return path

    def _move_remi(self, remi):
        """
        This function moves Remi of one tile in his direction, updates his
        position and his orientation.

        Parameters
        ----------
        remi    : Remi
            Remi
        orient : Orientation
            the new orientation of remi

        Returns
        -------
        None
        """
        if remi.get_orient() == Orientation.NORTH:
            # Case 1: no interaction with the limit of the board
            if remi.get_pos()[0] > 0:
                remi.update_pos_remi([remi.get_pos()[0]-1, remi.get_pos()[1]])
            else:
                # Case 2: interactions with the limit of the board (sic)
                if remi.get_pos()[1] % 2 == 1:
                    # If odd, go back one tile left and turn around
                    remi.update_pos_remi([0, remi.get_pos()[1]-1])
                    remi.update_remi(Orientation.SOUTH)
                elif remi.get_pos()[1] % 2 == 0 and remi.get_pos()[1] < (self.size-1):
                    # If even and not last, go right and turn around
                    remi.update_pos_remi([0, remi.get_pos()[1]+1])
                    remi.update_remi(Orientation.SOUTH)
                else:
                    # If even and last, turn left on first row
                    remi.update_pos_remi([0, self.size-1])
                    remi.update_remi(Orientation.WEST)

        elif remi.get_orient() == Orientation.SOUTH:
            # Case 1: no interaction with the limit of the board
            if remi.get_pos()[0] < self.size-1:
                remi.update_pos_remi([remi.get_pos()[0] +1, remi.get_pos()[1]])
            else:
                # Case 2: interactions with the limit of the board (sic)
                if remi.get_pos()[1] % 2 == 0 and remi.get_pos()[1] > 0:
                    # If even and not 0, go left one tile and turn around
                    remi.update_pos_remi([self.size-1, remi.get_pos()[1]-1])
                    remi.update_remi(Orientation.NORTH)
                elif remi.get_pos()[1] % 2 == 1:
                    # If odd, go right and turn around
                    remi.update_pos_remi([self.size-1, remi.get_pos()[1]+1])
                    remi.update_remi(Orientation.NORTH)
                else:
                    # If zero, turn right on first last
                    remi.update_pos_remi([self.size-1, 0])
                    remi.update_remi(Orientation.EAST)

        elif remi.get_orient() == Orientation.WEST:
            # Case 1: no interaction with the limit of the board
            if remi.get_pos()[1] > 0:
                remi.update_pos_remi([remi.get_pos()[0], remi.get_pos()[1]-1])
            else:
                # Case 2: interactions with the limit of the board (sic)
                if remi.get_pos()[0] % 2 == 0 and remi.get_pos()[0]<self.size-1:
                    # If even and not last, go south one tile and turn around
                    remi.update_pos_remi([remi.get_pos()[0]+1,0])
                    remi.update_remi(Orientation.EAST)
                elif remi.get_pos()[0] % 2 == 1:
                    # If odd, go up and turn around
                    remi.update_pos_remi([remi.get_pos()[0]-1, 0])
                    remi.update_remi(Orientation.EAST)
                else:
                    # If even and last, turn up on first column
                    remi.update_pos_remi([self.size-1, 0])
                    remi.update_remi(Orientation.NORTH)

        elif remi.get_orient() == Orientation.EAST:
            # Case 1: no interaction with the limit of the board
            if remi.get_pos()[1] < self.size-1:
                remi.update_pos_remi([remi.get_pos()[0], remi.get_pos()[1]+1])
            else:
                # Case 2: interactions with the limit of the board (sic)
                if remi.get_pos()[0] % 2 == 1:
                    # If odd, go south one tile and turn around
                    remi.update_pos_remi([remi.get_pos()[0]+1, self.size-1])
                    remi.update_remi(Orientation.WEST)
                elif remi.get_pos()[0] % 2 == 0 and remi.get_pos()[0] > 0:
                    # If even and not first, go up and turn around
                    remi.update_pos_remi([remi.get_pos()[0]-1, self.size-1])
                    remi.update_remi(Orientation.WEST)
                else:
                    # If even and first, turn south on last column
                    remi.update_pos_remi([0, self.size-1])
                    remi.update_remi(Orientation.SOUTH)

        # Debug
        #print(remi.get_pos())
