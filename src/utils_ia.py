from io import StringIO
import numpy as np
from orientation import Orientation

import game_logic
from board import Board
from remi import Remi
from game_exceptions import *

def board_from_csv_string(str_board):
    """
    > Example:
    scsv = '''
    -1, 0, -1
    -1, -1, -1
    1, 1, 1
    '''

    board_from_csv_string(scsv)
    """
    f = StringIO(str_board)
    arboard = np.loadtxt(f, delimiter=',')

    if arboard.shape[0] != arboard.shape[1]:
        raise ValueError("the board should be square")

    return Board(arboard.shape[0], arboard)


def simulate_round(player_1, board, remi, list_money_players, dice=None):
    print("Player 0 (you) is playing")

    if dice == None:
        dice = game_logic.roll_dice()

    #-- 1. get orientation from player_1
    orientation = player_1.get_direction(
        remi, \
        board, \
        list_money_players \
        )
    game_logic.assert_move_remi_is_legit(remi, orientation)

    #-- 2. roll dice and move
    remi.update_remi(orientation)
    board.move_remi(remi, dice)

    # 3. update google scholars
    index_player_below_remi = board.get_tile_player(remi.get_pos())
    if index_player_below_remi == board.NO_PAPER:
        pass
    else:
        if index_player_below_remi != 0:
            size = board.get_connex_paper(remi.pos, index_player_below_remi)
            print("you give " + str(size) + " citations to player " + str(index_player_below_remi))

    # 4. get player choice
    new_paper = player_1.place_paper(\
                remi, \
                board, \
                list_money_players \
                )
    game_logic.assert_paper_position_is_legit(board, remi, new_paper)

    # 5. end: update board with new paper
    board.add_paper(new_paper, 0)


def predict_move(pos, orient, dice, size=7):
    """
    A friendly function to predict the movement of Remi depending on
    the position on the board, the orientation and the number of steps.
    It returns the predicted position and orientation of Remi (but does not
    move him; in fact you cannot move Remi on your own :) )
    """
    new_pos = pos
    new_orient = orient
    for count in range(dice):
        new_pos, new_orient = _move_one(new_pos,new_orient, size)
    return new_pos, new_orient


def get_next_tiles(indices, board):
    """
    This function returns the positions and values of tiles next to the
    paper with indexes 'indices'.

    output
    ---------------
    out_pos: a list of list with the indexes of all adjacent tiles to paper
    with indexes 'indices'
    out_label: labels of each tile in out_pos
    """
    out_pos = []
    out_label = []
    for l in indices:
        # Check all tiles in current convex set
        if l[0]>0:
            # If current tile is not top border, explore top
            if [l[0]-1, l[1]] not in indices:
                # if top is not the second tile of the paper, add to list
                out_pos.append([l[0]-1,l[1]])
                out_label.append(board.get_tile_player([l[0]-1,l[1]]))

        if l[0]<board.size-1:
            # If current tile is not bottom border, explore below
            if [l[0]+1, l[1]] not in indices:
                out_pos.append([l[0]+1,l[1]])
                out_label.append(board.get_tile_player([l[0]+1,l[1]]))


        if l[1]>0:
            # If current tile is not left border, explore left
            if [l[0], l[1]-1] not in indices:
                out_pos.append([l[0],l[1]-1])
                out_label.append(board.get_tile_player([l[0],l[1]-1]))

        if l[1]<board.size-1:
            # If current tile is not right border, explore right
            if [l[0], l[1]+1] not in indices:
                out_pos.append([l[0],l[1]+1])
                out_label.append(board.get_tile_player([l[0],l[1]+1]))

    return out_pos, out_label


def  possible_place(ind, size=7):
    """
    A friendly function to get all the possible positions of papers on the
    board given a position of Remi and a board size (default=7).

    indi: list [i,j] stating the starting point
    size: integer, giving the size of the square board

    Returns a list of list of list (n by 2 by 2) with the n possibilities
    for placing papers on two connected tiles
    """
    poss_list = []
    # Lefties
    poss_list.append([[ind[0],ind[1]-1],[ind[0],ind[1]-2]])
    poss_list.append([[ind[0],ind[1]-1],[ind[0]-1,ind[1]-1]])
    poss_list.append([[ind[0],ind[1]-1],[ind[0]+1,ind[1]-1]])
    # Righties
    poss_list.append([[ind[0],ind[1]+1],[ind[0],ind[1]+2]])
    poss_list.append([[ind[0],ind[1]+1],[ind[0]-1,ind[1]+1]])
    poss_list.append([[ind[0],ind[1]+1],[ind[0]+1,ind[1]+1]])
    # Top
    poss_list.append([[ind[0]-1,ind[1]],[ind[0]-2,ind[1]]])
    poss_list.append([[ind[0]-1,ind[1]],[ind[0]-1,ind[1]-1]])
    poss_list.append([[ind[0]-1,ind[1]],[ind[0]-1,ind[1]+1]])
   # Bottom
    poss_list.append([[ind[0]+1,ind[1]],[ind[0]+2,ind[1]]])
    poss_list.append([[ind[0]+1,ind[1]],[ind[0]+1,ind[1]-1]])
    poss_list.append([[ind[0]+1,ind[1]],[ind[0]+1,ind[1]+1]])

    poss_list_filt = poss_list.copy()

    idx = -1
    # Filtering impossible positions
    for l in poss_list:
        idx += 1
        flag = 0
        # Testing each paper
        for tile in l:
            # Testing each tile
            if tile[0] < 0 or tile[0] > size-1:
                flag = 1
            if tile[1] < 0 or tile[1] > size-1:
                flag = 1
        if flag == 1:
            del poss_list_filt[idx]
            # Since we removed one line, index also decreases
            idx -= 1

    return poss_list_filt


def _move_one(pos, orient, size=7):
    """ This routines moves a fictional remi of one
    tile in the specified direction, updates his position and his orientation.

    Parameters
    ----------
    pos   : pos
        a list containing the (x,y) position of Rémi
    orient : Orientation
        the new orientation of remi

    Returns
    -------
    position and orientation after one step
    """
    if orient == Orientation.NORTH:
        # Case 1: no interaction with the limit of the board
        if pos[0] > 0:
            return [pos[0]-1, pos[1]], orient
        else:
            # Case 2: interactions with the limit of the board (sic)
            if pos[1] % 2 == 1:
                # If odd, go back one tile left and turn around
                return [0, pos[1]-1], Orientation.SOUTH
            elif pos[1] % 2 == 0 and pos[1] < (size-1):
                # If even and not last, go right and turn around
                return [0, pos[1]+1], Orientation.SOUTH
            else:
                # If even and last, turn left on first row
                return [0, size-1], Orientation.WEST

    elif orient == Orientation.SOUTH:
        # Case 1: no interaction with the limit of the board
        if pos[0] < size-1:
            return [pos[0] +1, pos[1]], orient
        else:
            # Case 2: interactions with the limit of the board (sic)
            if pos[1] % 2 == 0 and pos[1] > 0:
                # If even and not 0, go left one tile and turn around
                return [size-1, pos[1]-1], Orientation.NORTH
            elif pos[1] % 2 == 1:
                # If odd, go right and turn around
                return [size-1, pos[1]+1], Orientation.NORTH
            else:
                # If zero, turn right on first last
                return [size-1, 0], Orientation.EAST

    elif orient == Orientation.WEST:
        # Case 1: no interaction with the limit of the board
        if pos[1] > 0:
            return [pos[0], pos[1]-1], orient
        else:
            # Case 2: interactions with the limit of the board (sic)
            if pos[0] % 2 == 0 and pos[0] < size-1:
                # If even and not last, go south one tile and turn around
                return [pos[0]+1,0], Orientation.EAST
            elif pos[0] % 2 == 1:
                # If odd, go up and turn around
                return [pos[0]-1, 0], Orientation.EAST
            else:
                # If even and last, turn up on first column
                return [size-1, 0], Orientation.NORTH

    elif orient == Orientation.EAST:
        # Case 1: no interaction with the limit of the board
        if pos[1] < size-1:
            return [pos[0], pos[1]+1], orient
        else:
            # Case 2: interactions with the limit of the board (sic)
            if pos[0] % 2 == 1:
                # If odd, go south one tile and turn around
                return [pos[0]+1, size-1], Orientation.WEST
            elif pos[0] % 2 == 0 and pos[0] > 0:
                # If even and not first, go up and turn around
                return [pos[0]-1, size-1], Orientation.WEST
            else:
                # If even and first, turn south on last column
                return [0, size-1], Orientation.SOUTH
