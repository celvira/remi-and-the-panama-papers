import numpy
import argparse
import importlib.util

from game import Game

from utils import load_from_yaml

def load_player_from_module_path(path_to_module):
    spec = importlib.util.spec_from_file_location('module.name', path_to_module)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module

def main(args):
    do_verb = not args['no_print']
    params = load_from_yaml(args['game_params_yml'])

    p1_module = load_player_from_module_path(args['player1_pyfile'])
    p2_module = load_player_from_module_path(args['player2_pyfile'])

    p1 = p1_module.Player(name='Clement', idx=0, cits = params['n_citations'], shut_up = do_verb)
    p2 = p2_module.Player(name='Jeremy',  idx=1, cits = params['n_citations'], shut_up = do_verb)
    player_vect = [p1, p2]

    g = Game(player_vect, do_verb,
             n_round_init=params['n_papers'],
             n_citations_init=params['n_citations'])
    score, winner_idx = g.run_game()
    return score, winner_idx

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p1','--player1-pyfile', type=str, help="path to player 1 .py file")
    parser.add_argument('-p2','--player2-pyfile', type=str, help="path to player 2 .py file")
    parser.add_argument('-g','--game-params-yml', type=str, help="path to player 2 .yml file")
    parser.add_argument('-n','--no-print', action='store_true', help="if specified, remove printing")
    args = vars(parser.parse_args())
    if (not args['game_params_yml']):
        print('Default game parameters')
        args['game_params_yml'] = './data/game_params.yml'
    if (not args['player1_pyfile']) or (not args['player2_pyfile']):
        print('No Pythonic player provide... Default and silly one will be used:')
        p1 = 'stupid_player1.py'
        p2 = 'my_awesome_player.py'
        args['player1_pyfile'] = './src/players/' + p1
        args['player2_pyfile'] = './src/players/' + p2
        print('  MATCH: ' + p1 + ' VS ' + p2)
    main(args)
