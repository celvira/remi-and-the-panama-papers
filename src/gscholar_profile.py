class GScholarProfile(object):
    """docstring for GScholarProfile"""
    def __init__(self, budget):
        self.sum = budget

    def get_citations(self):
        return self.sum

    def isEmpty(self):
        return self.sum == 0

    def push(self, p):
        self.sum += p

    def pop(self, p):
        if self.sum >= p:
            self.sum -= p
            return p

        else:
            out = self.sum
            self.sum = 0
            return out
