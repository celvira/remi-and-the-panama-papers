import pickle
import yaml

def save_to_pickle(obj, filename):
    with open(filename, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_from_pickle(filename):
    with open(filename, 'rb') as f:
        return pickle.load(f)

def load_from_yaml(filename):
    with open(filename) as f:
        params = yaml.safe_load(f)
    return params
