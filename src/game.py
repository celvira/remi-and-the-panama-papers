import os

from board import Board
from remi import Remi
import game_logic
from game_exceptions import *
from gscholar_profile import GScholarProfile

from utils import save_to_pickle


class GameStatus(object):
    def __init__(self):
        super(GameStatus, self).__init__()
        self.orientation = None
        self.dice = None
        self.remi = None
        self.citations = None
        self.new_paper = None
        self.winner = None
        self.board  = None
        self.remi_path = None

def argmax(lst):
     return lst.index(max(lst))

class Game(object):
    """docstring for Game"""
    def __init__(self, player_vect, verbosity=True,
                 n_round_init=15, n_citations_init=30):
        super(Game, self).__init__()
        self.verbosity = verbosity

        citations_at_the_beginning = n_citations_init

        self.board = Board()
        self.remi = Remi()
        self.vec_players = player_vect
        self.vec_scprofiles \
            = [GScholarProfile(citations_at_the_beginning) for i in range(len(player_vect))]
        self.n_turns_left = n_round_init


    def get_player_from_index(self, index):
        return self.vec_players[index]

    def run_game(self):
        status_list = []
        it = 0
        while self.n_turns_left > 0:
            g_status = self.g_round(it)
            status_list.append((it, g_status))
            self.n_turns_left -= 1
            it += 1

        scores = []
        for profile in self.vec_scprofiles:
            scores.append(profile.get_citations())
        self._save_game(status_list)
        if self.verbosity: print("The winner is: Player" + str(argmax(scores)))
        return scores, argmax(scores)


    def g_round(self, g):
        g_status = []
        for i in range(len(self.vec_players)):
            round_str = '| ' + 'ROUND ' + str(g) + ' |'
            round_str = "+ " + round_str.center(50, "-") + " +"
            if self.verbosity: print(round_str)
            player = self.vec_players[i]
            status = self.p_round(player)
            g_status.append((i, status))
            if self.verbosity: self._print_game_status(g, i, status)
            if self.verbosity:  print('>'*28 + '<'*28)
        return g_status


    def p_round(self, player):
        i = player.idx

        # 0. status
        status = GameStatus()
        # 1. get orientation from player
        try:
            orientation = player.get_direction(\
                self.remi.copy(), \
                self.board.copy(), \
                [w.get_citations() for w in self.vec_scprofiles] \
                )
            game_logic.assert_move_remi_is_legit(self.remi, orientation)

        except OrientationException:
            print("Player " + str(i) + " move was not legit...")
            orientation = player.get_random_direction(self.remi.copy())

        except:
            print("Player " + str(i) + ": something wrong with your get_direction method")
            if self.verbosity: print("A random direction is chosen instead...")
            orientation = player.get_random_direction(self.remi.copy())

        status.orientation = orientation

        # 2. roll dice and move
        dice = game_logic.roll_dice()
        status.dice = dice

        path = self._move_remi(orientation, dice)
        status.remi = self.remi.copy()
        status.remi_path = path

        # 3. update google scholars
        index_player_below_remi = self.board.get_tile_player(self.remi.get_pos())
        if index_player_below_remi == self.board.NO_PAPER:
            pass
        else:
            #print("index " + str(index_player_below_remi))
            target_player = self.get_player_from_index(index_player_below_remi)
            if i != index_player_below_remi:
                size = self.board.get_connex_paper(self.remi.pos, index_player_below_remi)
                # Debug
                if self.verbosity: print("Player: ", player.idx,'lost',size)
                self._update_gsc(player, target_player, size)
        status.citations = [p.citations for p in self.vec_players]
        status.winner =  self.vec_players[argmax(status.citations)].name

        # 4. get player choice
        try:
            new_paper = player.place_paper(\
                self.remi.copy(), \
                self.board.copy(), \
                [w.get_citations() for w in self.vec_scprofiles] \
                )
            game_logic.assert_paper_position_is_legit(self.board, self.remi, new_paper)

        except PaperPositionException:
            print("Player " + str(i) + " paper move was not legit...")
            new_paper = player.place_paper_random(\
                self.remi.copy(), \
                self.board.copy()\
                )

        except:
            print("Player " + str(i) + ": something wrong with your place_paper method")
            if self.verbosity: print("A random position is chosen instead...")
            new_paper = player.place_paper_random(\
                self.remi.copy(), \
                self.board.copy()\
                )
        status.new_paper = new_paper.copy()

        # 5. end: update board with new paper
        self.board.add_paper(new_paper, player.idx)
        status.board = self.board.copy()

        return status


    def _move_remi(self, orientation, dice):
        self.remi.update_remi(orientation)
        return self.board.move_remi(self.remi, dice)


    def _update_gsc(self, sad_player, happy_player, size):
        cit_out = self.vec_scprofiles[sad_player.idx].pop(size)
        self.vec_scprofiles[happy_player.idx].push(cit_out)

        sad_player.citations = self.vec_scprofiles[sad_player.idx].get_citations()
        happy_player.citations = self.vec_scprofiles[happy_player.idx].get_citations()


    def _save_game(self, status_list):
        save_to_dir = './saved_games/'
        if not os.path.exists(save_to_dir):
            os.makedirs(save_to_dir)
        save_to_pickle(status_list, save_to_dir+'game.pkl')

    def _print_game_status(self, gr, pr, status):
        print('Round', gr, '- Player', pr, '- Dice roll:', status.dice)
        print('Round', gr, '- Player', pr, '- Remi pos:',status.remi.pos)
        print('Round', gr, '- Player', pr, '- Remi orient:',status.remi.orient)
        print('Round', gr, '- Player', pr, '- Remi path:',status.remi_path)
        print('Round', gr, '- Player', pr, '- Citations:', status.citations)
        print('Round', gr, '- Player', pr, '- Paper placed in:', status.new_paper)
        print('Round', gr, '- Player', pr, '- Current winner:',status.winner)
        print('Round', gr, '- Player', pr, '- Board:')
        curr_tiles = status.board.tiles.copy()
        curr_pos = status.remi.pos
        curr_tiles[curr_pos[0],curr_pos[1]] += status.board.NO_REMI
        print(curr_tiles)
